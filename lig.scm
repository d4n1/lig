#!~/.guix-profile/bin/guile \
-e main -s
!#

;;; This file is part of Lig (Libre IoT Guile).
;;;
;;; Lig is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Lesser General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Lig is distributed in the hope that it will be useful, but WITHOUT
;;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General
;;; Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with Lig.  If not, see
;;; <http://www.gnu.org/licenses/>.

(use-modules (ice-9 getopt-long))

(define (connect args)
  (let ((s (socket PF_INET SOCK_STREAM 0)))
    (connect s AF_INET (inet-pton AF_INET (car args)) 80)
    (display "GET / HTTP/1.0\r\n\r\n" s)

    (do ((line (read-line s) (read-line s)))
	((eof-object? line))
      (display line)
      (newline))))

(define (version)
  (display "\
Lig (Libre IoT Guile) 0.01
Copyright (C) 2016 Daniel Pimentel

License GPLv3+: GNU GPL 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
"))

(define (help)
  (display "\
Usage: lig [OPTION]... [CODE]...
Evaluate code with Lig, interactively or from a script.

  -c     connect socket
  -v     display this help and exit
  -h     display version information and exit

Report bugs to: d4n1@d4n1.org
Lig home page: <http://lig.nongnu.org>
"))

(define (main args)
  (let* ((option-spec '((connect (single-char #\c) (value #f))
			(version (single-char #\v) (value #f))
			(help (single-char #\h) (value #f))))
	 (options (getopt-long args option-spec))
	 (connect-option (option-ref options 'connect #f))
	 (version-option (option-ref options 'version #f))
	 (help-option (option-ref options 'help #f)))
    (if (or connect-option version-option help-option)
	(begin
	  (if connect-option (connect (cddr args)))
	  (if version-option (version))
	  (if help-option (help)))
	(begin
	  (version)
	  (newline)))))
