# Lig

Lig (Libre IoT Guile) is a project under LGPLv3 (GNU LESSER GENERAL
PUBLIC LICENSE) about IoT (Internet of Things) using GNU Guile (GNU
Ubiquitous Intelligent Language for Extensions) and Libre Hardware.

## Requeriments

* guile

## Build

```
make
```

## Install

```
make install
```

## Runing

```
lig -c localhost or lig --connect localhost
lig -v or lig --version
lig -h or lig --help
```